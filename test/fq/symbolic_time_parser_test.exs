defmodule Fq.SymbolicTimeParserTest do
  use ExUnit.Case

  import Fq.SymbolicTimeParser, only: [parse: 1]
  
  test "illegal symbolic time, now" do 
    assert parse("now") == {:error, "now"}
  end

  test "illegal symbolc time, a timestamp" do
    assert parse("2012-11-10T09:06:02Z") == {:error, "2012-11-10T09:06:02Z"}
  end

  [
    {"one second", "1.second.ago", {:second, -1}},
  ] 
  |> Enum.each(fn {test_name, symbolic_expr, result} ->
    test(test_name) do
      assert parse(unquote(symbolic_expr)) == {:ok, unquote(Macro.escape(result))}
    end
  end)
end
