defmodule Fq.TimeCalsTest do
  use ExUnit.Case

  alias Fq.TimeCalc


  test "now function" do
    :meck.expect(NaiveDateTime, :utc_now, fn -> ~N[2016-01-31 23:50:07] end)
    assert TimeParser.now() == {{2016, 1, 31}, {23, 50, 7}}
  end

  test "from string" do
    assert TimeParser.parse("2012-11-10T09:06:02Z") == {{2012, 11, 10}, {9, 6, 2}}
  end

  [
    { "now", {{2016, 1, 31}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},

    { "10.seconds.ago", {{2016, 1, 31}, {23, 49, 57}}, ~N[2016-01-31 23:50:07]},
    { "12.minutes.later", {{2016, 2, 1}, {0, 2, 7}}, ~N[2016-01-31 23:50:07]},
    { "1.hour.later", {{2016, 2, 1}, {0, 50, 7}}, ~N[2016-01-31 23:50:07]},

    { "1.day.ago", {{2016, 1, 30}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},
    { "31.days.ago", {{2015, 12, 31}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},
    { "5.week.ago", {{2015, 12, 27}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},

    { "1.months.ago", {{2015, 12, 31}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},

    { "2.year.ago", {{2014, 1, 31}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},

    # Some Leap Year Fun
    { "1.month.later", {{2016, 2, 29}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},
    { "13.months.later", {{2017, 2, 28}, {23, 50, 7}}, ~N[2016-01-31 23:50:07]},
    { "1.year.later", {{2017, 2, 28}, {23, 50, 7}}, ~N[2016-02-29 23:50:07]},
    { "1.year.ago", {{2015, 2, 28}, {23, 50, 7}}, ~N[2016-02-29 23:50:07]},

    # Adding Things Together
    { "1.day + 3.seconds.ago", {{2016, 2, 28}, {23, 50, 4}}, ~N[2016-02-29 23:50:07]},
    { "3.hours +9.minute + 53.seconds.later", {{2016, 2, 28}, {0, 0, 0}}, ~N[2016-02-27 20:50:07]},
    { "3.hours +9.minutes + 53.seconds.later", {{2016, 2, 29}, {0, 0, 0}}, ~N[2016-02-28 20:50:07]},
    { "3.hours +9.minute + 53.second.later", {{2017, 3, 1}, {0, 0, 0}}, ~N[2017-02-28 20:50:07]},

    { "1.day+50.minute + 1.second.ago", {{2017, 2, 28}, {23, 59, 59}}, ~N[2017-03-02 00:50:00]},
    { "1.day+50.minute +1.second.ago", {{2020, 2, 28}, {23, 59, 59}}, ~N[2020-03-02 00:50:00]},

  ]
  |> Enum.each(fn {symbolic_date_time, result, mocked_now} ->
    test(symbolic_date_time) do

      :meck.expect(NaiveDateTime, :utc_now, fn -> unquote(Macro.escape(mocked_now)) end)
      assert TimeParser.parse(unquote(symbolic_date_time)) == unquote(Macro.escape(result))

    end
  end)
end
