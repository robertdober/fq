defmodule Fq.SymbolicTimeParser do

  use Fq.Types

  def simple_unit_rgx, do: "(\\d+) \\. (seconds? | minutes? | hours? | days? | weeks? | months? | years?)" 
  def final_rsh_rgx, do: "#{simple_unit_rgx} \\. (ago | later)"
  def legal_symbolic_rgx, do: ~r{ ( #{simple_unit_rgx} \s* (\+|-) \s* )* #{final_rsh_rgx} }x
  @spec parse( String.t ) :: either(time_offset, String.t)
  def parse(str)
  def parse(str) do
  end
end
