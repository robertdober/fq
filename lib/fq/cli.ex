defmodule Fq.Cli do

  alias Fq.Options

  def main(argv) do
    argv
    |> parse_args
    # |> process
  end


  @switches  [
    start: :string,
    end: :string,
    min: :integer,
    max: :integer,
    mime: :string,

    help: :boolean,
    version: :boolean
  ]

  @aliases [
    h: :help,
    v: :version
  ] 

  defp parse_args args do
    case OptionParser.parse(args, strict: @switches, aliases: @aliases) do
      {options, args, []} -> IO.inspect process(options, args)
      {_, _, errors}      -> IO.puts(:stderr, "Illegal options:\n #{inspect errors}")
    end
  end
  
  defp process options, args do
    options |> Enum.into(%Options{})
  end

end
