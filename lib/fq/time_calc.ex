# defmodule Fq.TimeCalc do

#   alias Fq.SymbolicTimeParser, as: Parser

#   use Fq.Types



#   @future_or_past_rgx ~r{ (\d+) \. (seconds? | minutes? | hours? | days? | weeks? | months? | years?) \. (ago | later) }x 

#   @doc """
#   Transforms Time.utc_now to a triple double
#   e.g.
#   "2015-01-23T23:50:07Z" 
#   to
#   {{2015, 1, 23}, {23, 50, 7}}
#   """
#   @spec now() :: erl_time 
#   def now do
#     NaiveDateTime.utc_now()
#     |> NaiveDateTime.to_erl()
#   end

#   @doc """
#   Computes the time from now by means of a symbolic expression and returns it
#   as a triple double
#   e.g.  if now is

#   "2015-01-23T23:50:07Z" 
#   then parse("1.day.ago") will return

#   {{2015, 1, 22}, {23, 50, 7}}

#   """
#   @spec parse( String.t ) :: erl_time
#   def parse symbolic_date_time do
#     Regex.run(@future_or_past_rgx, symbolic_date_time)
#     |> parse_symbolic(symbolic_date_time)
#   end

#   @spec parse_symbolic( maybe(String.t), String.t ) :: erl_time
#   defp parse_symbolic(match, symbolic_date_time)
#   defp parse_symbolic(nil, "now"), do: now()
#   defp parse_symbolic(nil, symbolic_date_time) do
#     with {:ok, result} <- NaiveDateTime.from_iso8601( symbolic_date_time ), do:
#     result |> NaiveDateTime.to_erl()
#   end
#   defp parse_symbolic([_, qty, unit, tense], _) do
#     with {int_qty, _} <- qty |> Integer.parse(),
#     offset <- offset_from_unit(unit),
#     sign   <- tense |> sign_of_tense() do
#       now() |> offset_tuple(offset, int_qty * sign)
#     end
#   end

#   @spec sign_of_tense( String.t ) :: number()
#   defp sign_of_tense(tense)
#   defp sign_of_tense("later"), do: 1
#   defp sign_of_tense(_), do: -1

#   @spec offset_from_unit( String.t ) :: time_offset
#   defp offset_from_unit(unit)
#   defp offset_from_unit("second"), do: {:second, 1}
#   defp offset_from_unit("seconds"), do: {:second, 1}
#   defp offset_from_unit("minute"), do: {:second, 60}
#   defp offset_from_unit("minutes"), do: {:second, 60}
#   defp offset_from_unit("hour"), do: {:second, 3600}
#   defp offset_from_unit("hours"), do: {:second, 3600}
#   defp offset_from_unit("day"), do: {:second, 3600 * 24}
#   defp offset_from_unit("days"), do: {:second, 3600 * 24}
#   defp offset_from_unit("week"), do: {:second, 3600 * 24 * 7}
#   defp offset_from_unit("weeks"), do: {:second, 3600 * 24 * 7}
#   defp offset_from_unit("month"), do: {:month, 1}
#   defp offset_from_unit("months"), do: {:month, 1}
#   defp offset_from_unit(_), do: {:year, 1}


#   @spec offset_tuple( erl_time, time_offset, number() ) :: erl_time
#   defp offset_tuple tuple, offset, factor
#   defp offset_tuple({{year, month, day}, t}, {:year, _}, factor), do: {{year + factor, month, day}, t} |> normalize_month()
#   defp offset_tuple({{year, month, day}, t}, {:month, _}, factor), do: add_monthes {{year, month, day}, t}, factor
#   defp offset_tuple(erl, {:second, count}, factor) do
#     with {:ok, naive_date_time} = NaiveDateTime.from_erl(erl) do
#       naive_date_time |> NaiveDateTime.add(factor * count) |> NaiveDateTime.to_erl()
#     end
#   end

#   @spec add_monthes( erl_time, number() ) :: erl_time
#   defp add_monthes({{year, month, day}, t}, factor) do 
#     with add <- month + factor do
#       cond do
#         add > 12 -> add_monthes({{year+1, month, day}, t}, factor-12)
#         add < 1  -> add_monthes({{year-1, month, day}, t}, factor+12)
#         true     -> {{year, add, day}, t} |> normalize_month()
#       end
#     end
#   end
    
#   @spec normalize_month( erl_time ) :: erl_time
#   defp normalize_month({{year, month, day}, t} = erl_time) do
#     if day > :calendar.last_day_of_the_month(year, month) do
#       {{year, month, :calendar.last_day_of_the_month(year, month)}, t}
#     else
#       erl_time
#     end
#   end 
# end
