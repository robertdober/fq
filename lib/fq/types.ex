defmodule Fq.Types do
  defmacro __using__  _options \\ [] do
    quote do
      @type maybe(t) :: nil | t
      @type either(left, right) :: {:ok, left} | {:error, right}

      @type erl_time :: {{number(), number(), number()}, {number(), number(), number()}}
      @type offset_kind :: :second | :month | :year
      @type time_offset :: {offset_kind, number()}
    end
  end
end
