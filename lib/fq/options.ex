defmodule Fq.Options do
  defstruct start: nil, 
            end: nil,
            min: 1,           # Ignore Empty Files by default
            max: nil,
            mime: nil         # All mime types for now
end
