defmodule Fq do
  @moduledoc """
  FileQuery

  Run with fq [options] [files]

  options are parsed to an Fq.Options struct and then all files
  are filtered accordingly.
  """

  @doc """

      iex> Fq.hello
      :world

  """
  def hello do
    :world
  end
end
